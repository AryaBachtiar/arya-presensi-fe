import { FaBattleNet } from "react-icons/fa";
import React, { useEffect, useState } from "react";
import {  Modal, Form, Button } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { useHistory } from "react-router-dom";
import { GoPerson } from "react-icons/go";
import { RiAccountPinCircleLine } from "react-icons/ri";

export default function Navbar() {
  const history = useHistory();
  const [user, setUser] = useState([]);
  const [show1, setShow1] = useState(false);

  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const getAll = async () => {
    await axios
      .get("http://localhost:3200/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi kesalahan " + error);
      });
  };
  console.log(user);
  useEffect(() => {
    getAll();
  }, []);
  const logout = () => {
    Swal.fire({
      icon: "success",
      title: " LogOut Berhasil!",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      localStorage.clear();
      window.location.reload();
    }, 1500);
    history.push("/")
  };


  return (
    <header aria-label="Site Header" class="border-b border-pink-600">
    <div
      class="mx-auto flex h-16 max-w-screen-2xl items-center justify-between sm:px-6 lg:px-8"
    >
      <div class="flex items-center">
      <button type="button" class="p-2 sm:mr-4 lg:hidden">
        <svg
          class="h-6 w-6"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            d="M4 6h16M4 12h16M4 18h16"
          />
        </svg>
      </button>
<div className='text-white text-6xl '>
<FaBattleNet/>
</div>

<div>
  <a  className='text-pink-600 text-2xl font-serif italic' href="/home">YTTA</a>
  </div> 
    </div>
  
      <div class="flex flex-1 items-center justify-end">
       
  
        <div class="ml-8 flex items-center">
          <div
            class="flex items-center "
          >

            
          

            <span class="text-pink-500">
              <div
                
                class="block border-transparent p-6 hover:border-pink-700"
              >
                
                <button className="btn text-white" onClick={handleShow1}>
                  <h3>
                    {" "}
                    <GoPerson />{" "}
                  </h3>
                </button>
  
                
              </div>
            </span>
  
            <div class="flex items-center gap-4">
      <a href="#">
        <span class="sr-only">Logo</span>

        <span class="h-10 w-20 rounded-lg bg-gray-200"></span>
      </a>
    </div>

    {localStorage.getItem("role") !== null ? (
          <button>
            <a class="btn text-red-600" onClick={logout}>
              Logout
            </a>
          </button>
        ) : (
          <a className="btn  text-lime-600" href="/login">
            login
          </a>
        )}
 
          </div>
        </div>
      </div>
      
        {/* Profil */}
        <Modal show={show1} onHide={handleClose1} animation={false}>
        <div class="w-54 bg-gray-800 shadow-lg rounded-xl dark:bg-gray-800">
    <img alt="profil" src="https://png.pngtree.com/thumb_back/fh260/background/20200714/pngtree-modern-double-color-futuristic-neon-background-image_351866.jpg" class="w-full mb-4 rounded-t-lg h-28"/>
    <div class="flex flex-col items-center justify-center p-4 -mt-16">
        <a href="#" class="relative block">
            <img alt="profil" src={user.foto} class="mx-auto object-cover rounded-full h-16 w-16 "/>
        </a>
        <p class="mt-2 text-xl font-medium text-gray-200 dark:text-white">
        {user.nama}
        </p>
        <p class="flex items-center text-xs text-gray-300">
            <RiAccountPinCircleLine/> 
            {user.email}
        </p>
        <p class="text-xs text-gray-300">
            FullStack dev
        </p>
        <a href="/profil">
        <div class="flex items-center justify-between w-full gap-4 mt-8">
            <button type="button" class="py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                Profil
            </button>
        </div>
        </a>
    </div>
</div>
      </Modal>
    </div>
  </header>
  
  )
}
