import React from 'react'
import './App.css'
import { BrowserRouter,Switch,Route} from "react-router-dom"
import Login from './Pages/Login'
import Register from './Pages/Register'
import Home from './Pages/Home'
import Navbar from './Component/Navbar'



export default function App() {
  return (
    <div className='App'>
       <BrowserRouter>
       
      <main>
        <Switch>
          <Route path="/home" component={Home} exact/>
          <Route path="/" component={Login} exact/>
          <Route path="/register" component={Register} exact/>
          <Route path="/navbar" component={Navbar} exact/>
        </Switch>
      </main>
     
      </BrowserRouter>
    </div>
  )
}
